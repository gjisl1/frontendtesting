var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var fs = require("fs");
const { exec } = require('child_process');


console.log("Hi there!! You are awesome :)");



app.set('view engine', 'ejs');
app.use(express.static(__dirname + "public"));
app.use(bodyParser.urlencoded({ extended: false }))

app.get("/", function(req,res){
	res.render('index');
	console.log("On the webpage");
});

app.post("/send_test", function(req,res){
	var test = req.body.test + " -o test_result -s > /dev/null" ;
	var resp = req.body.response;

	console.log(req.body.test);
	console.log(req.body.response);
	var id = makeid(test.length + resp.length)
	console.log("Id = " + id);
	fileer(test,resp, id);
	res.render("result", {id:id});
})


app.listen(4040, function(err){
	if(err)
	{
		console.log(err);
	}
	else{
		console.log("The server is running on localhost:4040");
	}
});

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < 25; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function fileer(test, resp, id)
{
	fs.writeFile("W" + id+"_test", test, function(err){
		if(err)
		{
			console.log(err);
		}
		else{
			console.log("Saved for testcase");
		}
	});
	fs.writeFile("W"+id+"_response", resp, function(err){
		if(err)
		{
			console.log(err);
		}
		else{
			console.log("Saved for response");
		}
	});

	exec('ansible dev -m copy -a src=\"W'+id+"_response dest=W"+id+"_response\"", (err, stdout, stderr) => {
	  if (err) {
	    //some err occurred
	    console.error(err)
	  } else {
	   // the *entire* stdout and stderr (buffered)
	   console.log(`stdout: ${stdout}`);
	   console.log(`stderr: ${stderr}`);
	  }
	});
	exec('ansible dev -m copy -a src=\"W'+id+"_test dest=W"+id+"_test\"", (err, stdout, stderr) => {
	  if (err) {
	    //some err occurred
	    console.error(err)
	  } else {
	   // the *entire* stdout and stderr (buffered)
	   console.log(`stdout: ${stdout}`);
	   console.log(`stderr: ${stderr}`);
	  }
	});
	exec('ansible dev -m shell -a \"echo W' +id+' >> ~/teslist \"' , (err, stdout, stderr) => {
	  if (err) {
	    //some err occurred
	    console.error(err)
	  } else {
	   // the *entire* stdout and stderr (buffered)
	   console.log(`stdout: ${stdout}`);
	   console.log(`stderr: ${stderr}`);
	  }
	});
}


